$(document).ready(function(){

    $("#hero-slider form legend").Morphext({
        animation: "fadeIn",
        separator: "|",
        speed: 8000,
        complete: function () {}
    });

    var testimonials = $('.testimonials .quotes');
    testimonials.owlCarousel({
        items: 1,
        loop: true,
        nav: false,
    });
    $('.testimonials .right-arrow').click(function() {
        testimonials.trigger('next.owl.carousel');
        return false;
    });
    $('.testimonials .left-arrow').click(function() {
        testimonials.trigger('prev.owl.carousel', [300]);
        return false;
    });

    var article_carousel = $('.articles-carousel');
    article_carousel.owlCarousel({
        items: 3,
        nav: false,
        margin: 30,
        responsive : {
            0 : {
                items: 1
            },
            480 : {
                items: 2
            },
            768 : {
                items: 3
            }
        }
    });

    var hospital_gallery = $('.hospital-gallery');
    hospital_gallery.owlCarousel({
        items: 3,
        nav: false,
        margin: 10,
        responsive:{
            0:{
                items: 1
            },
            768:{
                items: 2
            },
            992:{
                items: 3
            }
        }
    });

    $('.hospital-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
    });
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){

    var star_value = $(this).data('value');
    $('#review_rate').val(star_value);

    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
  });
      

})