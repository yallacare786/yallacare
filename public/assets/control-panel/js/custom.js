$(document).ready(function(){
    // Qualifications
    $('body').on('click', '.add-qualification', function() {
        var card = $(this).closest('.card-body');
        var data = '<div class="form-row"> <div class="form-group col-5"> <label for="qualification" class="col-form-label">QUALIFICATION</label> <input type="text" class="form-control" name="qualification_name[]" id="qualification" placeholder="Qualification"> </div><div class="form-group col-5"> <label for="qualification_year" class="col-form-label">YEAR</label> <input type="text" class="form-control" name="qualification_year[]" id="qualification_year[]" placeholder="Pass Out Year"> </div><div class="form-group col-2 d-flex align-items-end justify-content-center"> <a class="btn btn-danger remove-qualification"><i class="icon-trash-o"></i></a> </div></div>';
        $(card).append(data);
        return false;
    });

    $('body').on('click', '.remove-qualification', function() {
        $(this).closest('.form-row').remove();
        return false;
    });
    // END Qualifications

    // Services
    $('body').on('click', '.add-service', function() {
        var card = $(this).closest('.card-body');
        var data = '<div class="form-row"> <div class="form-group col-4"> <label for="service" class="col-form-label">SERVICE</label> <input type="text" class="form-control" name="service_name[]" id="service" placeholder="Service"> </div><div class="form-group col-4"> <label for="experience" class="col-form-label">EXPERIENCE</label> <input type="text" class="form-control" name="service_experience[]" id="experience" placeholder="Experience"> </div><div class="form-group col-2"> <label for="price" class="col-form-label">PRICE</label> <input type="text" class="form-control" name="service_price[]" id="price" placeholder="Price"> </div><div class="form-group col-2 d-flex align-items-end justify-content-center"> <a class="btn btn-danger remove-service"><i class="icon-trash-o"></i></a> </div></div>';
        $(card).append(data);
        return false;
    });

    $('body').on('click', '.remove-service', function() {
        $(this).closest('.form-row').remove();
        return false;
    });
    // END Services

    // Specialites
    $('body').on('click', '.add-insurance', function() {
        var card = $(this).closest('.card-body');
        var data = '<div class="form-row"> <div class="form-group col-10"> <label for="insurance" class="col-form-label">INSURANCE</label> <input type="text" class="form-control" name="insurance[]" id="insurance" placeholder="Insurance"> </div><div class="form-group col-2 d-flex align-items-end justify-content-center"> <a class="btn btn-danger remove-insurance"><i class="icon-trash-o"></i></a> </div></div>';
        $(card).append(data);
        return false;
    });

    $('body').on('click', '.remove-insurance', function() {
        $(this).closest('.form-row').remove();
        return false;
    });
    // END Specialites

    // Amenties
    $('body').on('click', '.add-amenties', function() {
        var card = $(this).closest('.card-body');
        var data = '<div class="form-row"> <div class="form-group col-10"> <input type="text" class="form-control" name="amenties[]" id="amenties" placeholder="Amenties"> </div><div class="form-group col-2 d-flex align-items-end justify-content-center"> <a class="btn btn-danger remove-amenties"><i class="icon-trash"></i></a> </div></div>';
        $(card).append(data);
        return false;
    });

    $('body').on('click', '.remove-amenties', function() {
        $(this).closest('.form-row').remove();
        return false;
    });
    // END Amenties

    // Awards
    $('body').on('click', '.add-award', function() {
        var card = $(this).closest('.card-body');
        var data = '<div class="form-row"> <div class="form-group col-5"> <label for="award_name" class="col-form-label">Award name</label> <input type="text" class="form-control" name="award_name[]" id="award_name" placeholder="Award name"> </div><div class="form-group col-5"> <label for="award_year" class="col-form-label">Award year</label> <input type="text" class="form-control" name="award_year[]" id="award_year" placeholder="Award year"> </div><div class="form-group col-2 d-flex align-items-end justify-content-center"> <a class="btn btn-danger remove-award"><i class="icon-trash"></i></a> </div></div>';
        $(card).append(data);
        return false;
    });

    $('body').on('click', '.remove-award', function() {
        $(this).closest('.form-row').remove();
        return false;
    });
    // END Awards

})