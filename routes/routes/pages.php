<?php

/*
 * Admin Area
 */

$app->group('/admin/pages', function() {

    $this->get('/','PageController:list')->setName('pages.list');
    $this->get('/create', 'PageController:getCreate')->setName('create.new.page');
    $this->post('/create', 'PageController:create');
    $this->get('/update/{id:[0-9]+}', 'PageController:getUpdate')->setName('page.update');
    $this->put('/update/{id:[0-9]+}', 'PageController:update');
    $this->get('/delete/{id:[0-9]+}', 'PageController:delete')->setNAme('page.delete');

});