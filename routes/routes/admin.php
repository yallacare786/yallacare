<?php

use App\Middleware\AdminMiddleware;

$app->get('/admin/signin','AdminController:getSignIn')->setName('admin.signin');
$app->post('/admin/signin','AdminController:signIn');

$app->group('/admin', function() {

    $this->get('','AdminController:show')->setName('admin.index');

    /*
     * Users
    */
    $this->group('/users', function(){
        $this->get('/{type}','UserController:list')->setName('users.list');
        $this->get('/create/{type}', 'UserController:getCreate')->setName('create.new.user');
        $this->post('/create/{type}', 'UserController:create');
        $this->get('/update/{id:[0-9]+}', 'UserController:getUpdate')->setName('user.update');
        $this->put('/update/{id:[0-9]+}', 'UserController:update');
        $this->get('/delete/{id:[0-9]+}', 'UserController:delete')->setNAme('user.delete');
    });

    /*
     * Appointments
    */
    $this->group('/appointments', function(){
        $this->get('', 'AppointmentController:list')->setName('appointments.list');
        $this->get('/{id:[0-9]+}', 'AppointmentController:single')->setName('appointments.single');
        $this->put('/update/{id:[0-9]+}', 'AppointmentController:update')->setName('appointment.update');
        $this->get('/delete/{id:[0-9]+}', 'AppointmentController:delete')->setName('appointment.delete');
    });

    /*
     * Specialities
    */
    $this->group('/specialities', function(){
        $this->get('', 'SpecialityController:list')->setName('specialities.list');
        $this->post('/create', 'SpecialityController:create')->setName('create.new.speciality');
        $this->get('/update/{id:[0-9]+}', 'SpecialityController:getUpdate')->setName('speciality.update');
        $this->put('/update/{id:[0-9]+}', 'SpecialityController:update');
        $this->get('/delete/{id:[0-9]+}', 'SpecialityController:delete')->setName('speciality.delete');
    });

    /*
     * Hospitals
    */
    $this->group('/hospitals', function(){
        $this->get('/','HospitalController:list')->setName('hospitals.list');
        $this->get('/create/', 'HospitalController:getCreate')->setName('create.new.hospital');
        $this->post('/create/', 'HospitalController:create');
        $this->get('/update/{id:[0-9]+}', 'HospitalController:getUpdate')->setName('hospital.update');
        $this->put('/update/{id:[0-9]+}', 'HospitalController:update');
        $this->get('/delete/{id:[0-9]+}', 'HospitalController:delete')->setNAme('hospital.delete');
    });

    /*
     * Articles
    */
    $this->group('/articles', function(){
        $this->get('/','ArticleController:list')->setName('articles.list');
        $this->get('/create', 'ArticleController:getCreate')->setName('create.new.article');
        $this->post('/create', 'ArticleController:create');
        $this->get('/update/{id:[0-9]+}', 'ArticleController:getUpdate')->setName('article.update');
        $this->put('/update/{id:[0-9]+}', 'ArticleController:update');
        $this->get('/delete/{id:[0-9]+}', 'ArticleController:delete')->setNAme('article.delete');
    });

    /*
     * Categories
    */
    $this->group('/categories', function(){
        $this->get('', 'CategoryController:list')->setName('categories.list');
        $this->post('/create', 'CategoryController:create')->setName('create.new.category');
        $this->get('/update/{id:[0-9]+}', 'CategoryController:getUpdate')->setName('category.update');
        $this->put('/update/{id:[0-9]+}', 'CategoryController:update');
        $this->get('/delete/{id:[0-9]+}', 'CategoryController:delete')->setName('category.delete');
    });

    /*
     * Pages
    */
    $this->group('/pages', function(){
        $this->get('/','PageController:list')->setName('pages.list');
        $this->get('/create', 'PageController:getCreate')->setName('create.new.page');
        $this->post('/create', 'PageController:create');
        $this->get('/update/{id:[0-9]+}', 'PageController:getUpdate')->setName('page.update');
        $this->put('/update/{id:[0-9]+}', 'PageController:update');
        $this->get('/delete/{id:[0-9]+}', 'PageController:delete')->setNAme('page.delete');
    });

})->add(new AdminMiddleware($container));