<?php 

use App\Middleware\GuestMiddleware;

$app->get('/auth/signout', 'AuthController:signOut')->setName('auth.signout');

$app->group('/auth', function () {

    $this->get('/signup', 'AuthController:getSignUp')->setName('auth.signup');
    $this->post('/signup', 'AuthController:postSignUp');
    $this->get('/signin', 'AuthController:getSignIn')->setName('auth.signin');
    $this->post('/signin', 'AuthController:postSignIn');

})->add(new GuestMiddleware($container));