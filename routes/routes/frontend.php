<?php

use App\Middleware\AuthMiddleware;

/*
 * Home
 */

$app->get('/','HomeController:show')->setName('homepage');

/*
 * Blog
 */

$app->group('/blog', function() {

    $this->get('/','BlogController:list')->setName('blog.list');
    $this->get('/{id:[0-9]+}', 'BlogController:single')->setName('blog.single');
    $this->get('/category/{id:[0-9]+}','BlogController:category')->setName('blog.category');
    $this->get('/search','BlogController:search')->setName('blog.search');

});

/*
 * Hospitals
 */

$app->group('/hospitals', function() {

    $this->get('','HospitalController:show')->setName('hospitals.show');
    $this->get('/{id:[0-9]+}', 'HospitalController:single')->setName('hospitals.single');
    $this->get('/speciality/{id:[0-9]+}','HospitalController:speciality')->setName('hospitals.speciality');
    $this->get('/search','HospitalController:search')->setName('hospitals.search');
    $this->post('/send-enquiry/{id:[0-9]+}','HospitalController:sendEnquiry')->setName('hospital.send.enquiry');
    $this->post('/addcomment/{id:[0-9]+}','HospitalController:addComment')->setName('hospital.add.comment');

});

/*
 * Doctors
 */

$app->group('/doctors', function() {

    $this->get('','DoctorController:show')->setName('doctors.show');
    $this->get('/{id:[0-9]+}', 'DoctorController:single')->setName('doctors.single');
    $this->get('/speciality/{id:[0-9]+}','DoctorController:speciality')->setName('doctors.speciality');
    $this->get('/search','DoctorController:search')->setName('doctors.search');
    $this->post('/book/{id:[0-9]+}','DoctorController:book')->setName('doctor.book');
    $this->post('/addcomment/{id:[0-9]+}','DoctorController:addComment')->setName('doctor.add.comment');

});

/*
 * Patients
 */

$app->group('/patients', function() {

    $this->get('/{id:[0-9]+}', 'UserController:patientProfile')->setName('patient.profile');
    $this->get('/edit/{id:[0-9]+}', 'UserController:getEditPatient')->setName('edit.patient');
    $this->post('/edit/{id:[0-9]+}', 'UserController:editPatient');

})->add(new AuthMiddleware($container));

/*
 * Second Opinion
 */

$app->get('/second-opinion','SecondOpinionController:show')->setName('second.opinion');
$app->post('/second-opinion','SecondOpinionController:send');

/*
 * Contact
 */

$app->get('/contact','ContactController:show')->setName('contact');
$app->post('/contact','ContactController:send');

/*
 * About
 */

$app->get('/about','AboutController:show')->setName('aboutus');

/*
 * Location
 */

$app->get('/locations','LocationController:show')->setName('locations');

/*
 * How it works
 */

$app->get('/how-it-works','HowController:show')->setName('howitworks');