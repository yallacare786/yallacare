<?php

/*
 * Admin Area
 */

$app->group('/admin/hospitals', function() {

    $this->get('/','HospitalController:list')->setName('hospitals.list');
    $this->get('/create/', 'HospitalController:getCreate')->setName('create.new.hospital');
    $this->post('/create/', 'HospitalController:create');
    $this->get('/update/{id:[0-9]+}', 'HospitalController:getUpdate')->setName('hospital.update');
    $this->put('/update/{id:[0-9]+}', 'HospitalController:update');
    $this->get('/delete/{id:[0-9]+}', 'HospitalController:delete')->setNAme('hospital.delete');

});