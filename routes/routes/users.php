<?php

/*
 * Admin Area
 */

$app->group('/admin/users', function() {

    $this->get('/{type}','UserController:list')->setName('users.list');
    $this->get('/create/{type}', 'UserController:getCreate')->setName('create.new.user');
    $this->post('/create/{type}', 'UserController:create');
    $this->get('/update/{id:[0-9]+}', 'UserController:getUpdate')->setName('user.update');
    $this->put('/update/{id:[0-9]+}', 'UserController:update');
    $this->get('/delete/{id:[0-9]+}', 'UserController:delete')->setNAme('user.delete');

});