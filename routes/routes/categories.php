<?php 

$app->group('/admin/categories', function() {

    $this->get('', 'CategoryController:list')->setName('categories.list');
    $this->post('/create', 'CategoryController:create')->setName('create.new.category');
    $this->get('/update/{id:[0-9]+}', 'CategoryController:getUpdate')->setName('category.update');
    $this->put('/update/{id:[0-9]+}', 'CategoryController:update');
    $this->get('/delete/{id:[0-9]+}', 'CategoryController:delete')->setName('category.delete');

});