<?php

/*
 * Admin Area
 */

$app->group('/admin/articles', function() {

    $this->get('/','ArticleController:list')->setName('articles.list');
    $this->get('/create', 'ArticleController:getCreate')->setName('create.new.article');
    $this->post('/create', 'ArticleController:create');
    $this->get('/update/{id:[0-9]+}', 'ArticleController:getUpdate')->setName('article.update');
    $this->put('/update/{id:[0-9]+}', 'ArticleController:update');
    $this->get('/delete/{id:[0-9]+}', 'ArticleController:delete')->setNAme('article.delete');

});