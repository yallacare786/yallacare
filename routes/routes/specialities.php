<?php 

$app->group('/admin/specialities', function() {

    $this->get('', 'SpecialityController:list')->setName('specialities.list');
    $this->post('/create', 'SpecialityController:create')->setName('create.new.speciality');
    $this->get('/update/{id:[0-9]+}', 'SpecialityController:getUpdate')->setName('speciality.update');
    $this->put('/update/{id:[0-9]+}', 'SpecialityController:update');
    $this->get('/delete/{id:[0-9]+}', 'SpecialityController:delete')->setName('speciality.delete');

});