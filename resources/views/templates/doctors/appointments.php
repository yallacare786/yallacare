<?php include "header.php"; ?>

<?php include "sidebar.php"; ?>

<!-- TOP PAGE -->
<div class="has-sidebar-left">

    <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text" placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>

    <div class="sticky">
        <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
            <div class="relative">
                <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle"><i></i></a>
            </div>

            <!-- TOP MENU -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown custom-dropdown user user-menu ">
                        <a href="#" class="nav-link" data-toggle="dropdown">
                            <img src="assets/img/dummy/u8.png" class="user-image" alt="User Image">
                            <i class="icon-more_vert "></i>
                        </a>
                        <div class="dropdown-menu p-4 dropdown-menu-right">
                            <div class="row box justify-content-between my-4">
                                <div class="col">
                                    <a href="#">
                                        <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                        <div class="pt-1">Apps</div>
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                                        <div class="pt-1">Profile</div>
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                        <div class="pt-1">Settings</div>
                                    </a>
                                </div>
                            </div>
                            <div class="row box justify-content-between my-4">
                                <div class="col">
                                    <a href="#">
                                        <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                        <div class="pt-1">Apps</div>
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                                        <div class="pt-1">Profile</div>
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                        <div class="pt-1">Settings</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- END TOP MENU -->

        </div>
    </div>

</div>
<!-- END TOP PAGE -->

<!-- PAGE -->
<div class="page has-sidebar-left height-full">

    <!-- HEADER -->
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">

            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4> <i class="icon-user-md"></i> Doctors </h4>
                </div>
            </div>
            
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li class="float-right">
                        <a class="nav-link" href="doctors.php" ><i class="icon icon-home2"></i> All Doctors</a>
                    </li>
                    <li class="float-right">
                        <a class="nav-link" href="new-doctor.php" ><i class="icon icon-plus-circle"></i> Add New Doctor</a>
                    </li>
                    <li class="float-right">
                        <a class="nav-link active" href="doctor-appointments.php" ><i class="icon icon-calendar-check-o"></i> Appointments</a>
                    </li>
                    <li class="float-right">
                        <a class="nav-link" href="doctors-reviews.php" ><i class="icon icon-star"></i> Reviews</a>
                    </li>
                    <li class="float-right">
                        <a class="nav-link" href="doctors-trash.php" ><i class="icon icon-trash-can4"></i> Trash</a>
                    </li>
                </ul>
            </div>
                
        </div>
    </header>
    <!-- END HEADER -->

    <!-- BODY -->
    <div class="container-fluid">
        <div class="card r-0 shadow my-3 py-3">
            <div class="card-body">
                <div class="table-responsive">
                    <form>
                        <table class="table table-striped table-hover r-0 data-tables">
                            <thead>
                                <tr class="no-b">
                                    <th style="width: 30px">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="checkedAll" class="custom-control-input"><label class="custom-control-label" for="checkedAll"></label>
                                        </div>
                                    </th>
                                    <th>NAME</th>
                                    <th>DATE OF BOOKING</th>
                                    <th>TIME OF BOOKING</th>
                                    <th>DOCTOR NAME</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i = 0; $i < 12; $i++) : ?>
                                <tr>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input checkSingle" id="user_id_1" required><label class="custom-control-label" for="user_id_1"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="avatar avatar-md mr-3 mt-1 float-left">
                                            <span class="avatar-letter avatar-letter-h  avatar-md circle"></span>
                                        </div>
                                        <div>
                                            <div>
                                                <strong>Hamid Akhatar</strong>
                                            </div>
                                            <small>hamidhmdsakhatar@gmail.com</small>
                                        </div>
                                    </td>
                                    <td>28-07-2018</td>
                                    <td>03:05 PM</td>
                                    <td>Dr. Khaled Abu Taha</td>
                                    <td>
                                        <a href="single-doctor-appointment.php"><i class="icon-eye mr-3"></i></a>
                                        <a href="#"><i class="icon-trash-can4"></i></a>
                                    </td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                            <tfoot>
                                <tr class="no-b">
                                    <th style="width: 30px"></th>
                                    <th>NAME</th>
                                    <th>DATE OF BOOKING</th>
                                    <th>TIME OF BOOKING</th>
                                    <th>DOCTOR NAME</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END BODY -->

</div>
<!-- END PAGE -->

<?php include "footer.php"; ?>