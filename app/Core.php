<?php 

namespace App;

use Slim\Http\UploadedFile;

class Core
{
    public static function moveUploadedFile($directory, UploadedFile $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename  = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename  = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }
}

?>

<ul>
    <li><a href="#">Facebook</a></li>
    <li><a href="#">Twitter</a></li>
    <li><a href="#">Linkedin</a></li>
    <li><a href="#">Google+</a></li>
</ul>