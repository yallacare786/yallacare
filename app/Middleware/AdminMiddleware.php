<?php 

namespace App\Middleware;

class AdminMiddleware extends Middleware
{

    public function __invoke( $request, $response, $next )
    {
        if( !$this->container->auth->check() ) {
            return $response->withRedirect($this->container->router->pathFor('admin.signin'));
        }

        $user = $this->container->auth->user();

        if( $user->role_id != 3 ) {
            return $response->withRedirect($this->container->router->pathFor('admin.signin'));
        }

        $response = $next($request, $response);
        return $response;
    }

}