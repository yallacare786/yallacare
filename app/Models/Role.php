<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function get_role_by_name( $name ) {
        $results = self::where('role_name', $name)->first();
        return $results;
    }

    public static function get_admin_role() {
        $results = self::where('role_name', 'admin')->first();
        if( $results )
            return $results->id;
        return false;
    }

    public static function get_doctor_role() {
        $results = self::where('role_name', 'doctor')->first();
        if( $results )
            return $results->id;
        return false;
    }

    public static function get_patient_role() {
        $results = self::where('role_name', 'patient')->first();
        if( $results )
            return $results->id;
        return false;
    }

    public static function get_role_name_by_id($id) {
        $results = self::where('id', $id)->first();
        if( $results )
            return $results->role_name;
        return false;
    }

}