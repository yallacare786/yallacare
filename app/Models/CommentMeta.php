<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentMeta extends Model
{
    protected $table = 'comment_meta';
    protected $guarded = ['id','created_at','updated_at'];

    public static function add_meta($id, $meta_key, $meta_value)
    {
        $meta = self::updateOrCreate([
            'comment_id' => $id,
            'meta_key'   => $meta_key,
            'meta_value' => $meta_value
        ]);
        return $meta;
    }

    public static function update_meta($id, $meta_key, $meta_value)
    {
        $meta = self::where('comment_id', $id)->where('meta_key', $meta_key)->first();
        if( $meta == null ) {
            $add_new = self::add_meta($id, $meta_key, $meta_value);
            return $add_new->id;
        } else {
            if( $meta->meta_value != $meta_value ) {
                $meta->meta_value = $meta_value;
                $meta->save();
                return $meta->id;
            }
            return $meta->id;
        }
        return false;
    }

    public static function delete_meta($id, $meta_key)
    {
        $meta = self::where('comment_id', $id)->where('meta_key', $meta_key)->first();
        if( $meta != null ) {
            $delete = self::find($meta->id);
            $delete->delete();
            return true;
        }
        return false;
    }

    public static function get_meta($id, $meta_key, $value_only = false)
    {
        $meta = self::where('comment_id', $id)->where('meta_key', $meta_key)->first();
        if( $meta != null ) {
            if( $value_only == true )
                return $meta->meta_value;
            return $meta;
        }
        return false;
    }

}