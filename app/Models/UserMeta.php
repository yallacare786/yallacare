<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $table = 'user_meta';
    protected $guarded = ['id','created_at','updated_at'];

    public static function add_meta($user_id, $meta_key, $meta_value)
    {
        $user_meta = self::updateOrCreate([
            'user_id'    => $user_id,
            'meta_key'   => $meta_key,
            'meta_value' => $meta_value
        ]);
        return $user_meta;
    }

    public static function update_meta($user_id, $meta_key, $meta_value)
    {
        $user_meta = self::where('user_id', $user_id)->where('meta_key', $meta_key)->first();
        if( $user_meta == null ) {
            $add_new = self::add_meta($user_id, $meta_key, $meta_value);
            return $add_new->id;
        } else {
            if( $user_meta->meta_value != $meta_value ) {
                $user_meta->meta_value = $meta_value;
                $user_meta->save();
                return $user_meta->id;
            }
            return $user_meta->id;
        }
        return false;
    }

    public static function delete_meta($user_id, $meta_key)
    {
        $user_meta = self::where('user_id', $user_id)->where('meta_key', $meta_key)->first();
        if( $user_meta != null ) {
            $delete = self::find($user_meta->id);
            $delete->delete();
            return true;
        }
        return false;
    }

    public static function get_meta($user_id, $meta_key, $value_only = false)
    {
        $user_meta = self::where('user_id', $user_id)->where('meta_key', $meta_key)->first();
        if( $user_meta != null ) {
            if( $value_only == true )
                return $user_meta->meta_value;
            return $user_meta;
        }
        return false;
    }

}