<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryRelationship extends Model
{
    protected $table = 'categories_relationships';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}