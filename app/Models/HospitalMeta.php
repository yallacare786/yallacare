<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalMeta extends Model
{
    protected $table = 'hospital_meta';
    protected $guarded = ['id','created_at','updated_at'];

    public static function add_meta($hospital_id, $meta_key, $meta_value)
    {
        $hospital_meta = self::updateOrCreate([
            'hospital_id'    => $hospital_id,
            'meta_key'       => $meta_key,
            'meta_value'     => $meta_value
        ]);
        return $hospital_meta;
    }

    public static function update_meta($hospital_id, $meta_key, $meta_value)
    {
        $hospital_meta = self::where('hospital_id', $hospital_id)->where('meta_key', $meta_key)->first();
        if( $hospital_meta == null ) {
            $add_new = self::add_meta($hospital_id, $meta_key, $meta_value);
            return $add_new->id;
        } else {
            if( $hospital_meta->meta_value != $meta_value ) {
                $hospital_meta->meta_value = $meta_value;
                $hospital_meta->save();
                return $hospital_meta->id;
            }
            return $hospital_meta->id;
        }
        return false;
    }

    public static function delete_meta($hospital_id, $meta_key)
    {
        $hospital_meta = self::where('hospital_id', $hospital_id)->where('meta_key', $meta_key)->first();
        if( $hospital_meta != null ) {
            $delete = self::find($hospital_meta->id);
            $delete->delete();
            return true;
        }
        return false;
    }

    public static function get_meta($hospital_id, $meta_key, $value_only = false)
    {
        $hospital_meta = self::where('hospital_id', $hospital_id)->where('meta_key', $meta_key)->first();
        if( $hospital_meta != null ) {
            if( $value_only == true )
                return $hospital_meta->meta_value;
            return $hospital_meta;
        }
        return false;
    }

}