<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialityRelationship extends Model
{
    protected $table = 'specialities_relationships';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}