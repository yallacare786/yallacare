<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    protected $table = 'post_meta';
    protected $guarded = ['id','created_at','updated_at'];

    public static function add_meta($post_id, $meta_key, $meta_value)
    {
        $post_meta = self::updateOrCreate([
            'post_id'    => $post_id,
            'meta_key'   => $meta_key,
            'meta_value' => $meta_value
        ]);
        return $post_meta;
    }

    public static function update_meta($post_id, $meta_key, $meta_value)
    {
        $post_meta = self::where('post_id', $post_id)->where('meta_key', $meta_key)->first();
        if( $post_meta == null ) {
            $add_new = self::add_meta($post_id, $meta_key, $meta_value);
            return $add_new->id;
        } else {
            if( $post_meta->meta_value != $meta_value ) {
                $post_meta->meta_value = $meta_value;
                $post_meta->save();
                return $post_meta->id;
            }
            return $post_meta->id;
        }
        return false;
    }

    public static function delete_meta($post_id, $meta_key)
    {
        $post_meta = self::where('post_id', $post_id)->where('meta_key', $meta_key)->first();
        if( $post_meta != null ) {
            $delete = self::find($post_meta->id);
            $delete->delete();
            return true;
        }
        return false;
    }

    public static function get_meta($post_id, $meta_key, $value_only = false)
    {
        $post_meta = self::where('post_id', $post_id)->where('meta_key', $meta_key)->first();
        if( $post_meta != null ) {
            if( $value_only == true )
                return $post_meta->meta_value;
            return $post_meta;
        }
        return false;
    }

}