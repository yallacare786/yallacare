<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    
    protected $table = 'enquiries';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}