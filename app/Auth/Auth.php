<?php 

namespace App\Auth;

use App\Models\User;

class Auth
{
    public function user()
    {
        return User::find($_SESSION['user']);
    }

    public function is_admin()
    {
        if( $this->user()->role_id == 3 )
            return true;
        return false;
    }

    public function check()
    {
        return isset($_SESSION['user']);
    }

    public function attempt($email, $password)
    {
        $user = User::where('email', $email)->first();

        if( !$user ) {
            return false;
        }

        if( password_verify($password,$user->password) ) {
            $_SESSION['user'] = $user->id;
            return true;
        }

        return false;
    }

    public function admin_attempt($email, $password)
    {
        $user = User::where('email', $email)->first();

        if( !$user ) {
            return false;
        }

        if( password_verify($password,$user->password) ) {
            if( $user->role_id == 3 ) {
                $_SESSION['user'] = $user->id;
                return true;
            }
            return false;
        }

        return false;
    }

    public function logout()
    {
        unset($_SESSION['user']);
    }

}