<?php 

namespace App\Controllers;

use App\Core;
use App\Models\User;
use App\Models\Role;
use Respect\Validation\Validator as v;

class AuthController extends Controller 
{

    public function getSignUp($request, $response, $args)
    {
        return $this->view->render($response, 'templates/auth/signup.twig');
    }

    public function postSignUp($request, $response, $args)
    {
        
        $validation = $this->validator->validate($request, [
            'first_name'    => v::noWhitespace()->notEmpty(),
            'last_name'     => v::noWhitespace()->notEmpty(),
            'email'         => v::noWhitespace()->notEmpty()->emailAvailable(),
            'password'      => v::notEmpty(),
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('auth.signup'));
        }

        $role = Role::get_role_by_name('patient');

        $user = User::create([
            'first_name' => $request->getParam('first_name'),
            'last_name'  => $request->getParam('last_name'),
            'email'      => $request->getParam('email'),
            'password'   => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
            'role_id'    => $role->id
        ]);

        $_SESSION['old'] = '';
        $this->flash->addMessage('success', 'Your account has been created successfully');
        return $response->withRedirect($this->router->pathFor('auth.signup'));

    }

    public function getSignIn($request, $response, $args)
    {
        return $this->view->render($response, 'templates/auth/signin.twig');
    }

    public function PostSignIn($request, $response, $args)
    {

        $validation = $this->validator->validate($request, [
            'email'    => v::email()->notEmpty()->emailNotAvailable(),
            'password' => v::notEmpty(),
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        $auth = $this->auth->attempt($request->getParam('email'),$request->getParam('password'));
        if( !$auth ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }
        
        return $response->withRedirect($this->router->pathFor('homepage'));

    }

    public function signOut($request, $response, $args)
    {

        $this->auth->logout();
        return $response->withRedirect($this->router->pathFor('homepage'));

    }



}