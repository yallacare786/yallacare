<?php 

namespace App\Controllers;

use App\Core;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Hospital;
use App\Models\HospitalMeta;
use App\Models\Role;
use App\Models\Speciality;
use App\Models\SpecialityRelationship;
use App\Models\Enquiry;
use App\Models\comment;
use Respect\Validation\Validator as v;
use Illuminate\Database\Capsule\Manager as DB;

class HospitalController extends Controller
{

    public function list($request, $response, $args)
    {

        $count          = Hospital::count();   // Count of all available hospitals      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Hospitals on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $hospitals      = Hospital::skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/control-panel/templates/admin/hospitals.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'hospitals' => $hospitals,
        ]);

    }

    public function getCreate($request, $response, $args)
    {

        $specialities = Speciality::get();
        return $this->view->render($response, 'templates/control-panel/templates/admin/new-hospital.twig',['specialities' => $specialities]);

    }

    public function create($request, $response, $args)
    {

        $validation = $this->validator->validate($request, [
            'name'      => v::notEmpty(),
            'email'     => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'type'      => v::noWhitespace()->notEmpty()->alpha(),
            'password'  => v::notEmpty(),
            'phone'     => v::noWhitespace()->notEmpty()->phone(),
            'price'     => v::noWhitespace()->notEmpty(),
            'city'      => v::notEmpty(),
            'state'     => v::notEmpty(),
            'country'   => v::notEmpty(),
            'address'   => v::notEmpty(),
            'zip_code'  => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('create.new.hospital'));
        }
        
        $hospital = Hospital::create([
            'name'      => $request->getParam('name'),
            'email'     => $request->getParam('email'),
            'type'      => $request->getParam('type'),
            'password'  => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
            'phone'     => $request->getParam('phone'),
            'price'     => $request->getParam('price'),
            'city'      => $request->getParam('city'),
            'state'     => $request->getParam('state'),
            'country'   => $request->getParam('country'),
            'address'   => $request->getParam('address'),
            'zip_code'  => $request->getParam('zip_code'),
            'about'     => $request->getParam('about')
        ]);

        if( !empty($request->getParam('specialities')) ) {
            $specialities = [];
            foreach( $request->getParam('specialities') as $speciality ) {
                $sp = explode(',',$speciality);
                $specialities[] = [ 'speciality_id' => $sp[0], 'speciality_name' => $sp[1], 'object_id' => $hospital->id, 'object_type' => 'hospital'];
            }
            $insert_speciality = SpecialityRelationship::insert($specialities);
        }

        if( !empty($request->getParam('award_name')) ) {
            $awards = [];
            $count = count($request->getParam('award_name'));
            for( $i = 0; $i < $count; $i++ ) {
                $awards[] = [ 'award_name' => $request->getParam('award_name')[$i], 'award_year' => $request->getParam('award_year')[$i] ];
            }
            $awards_in_json = json_encode($awards);
            HospitalMeta::add_meta($hospital->id, 'awards', $awards_in_json);
        }

        if( !empty($request->getParam('amenties')) ) {
            $amenties_in_json = json_encode($request->getParam('amenties'));
            HospitalMeta::add_meta($hospital->id, 'amenties', $amenties_in_json);
        }

        $directory =  base_path('public/uploads/hospitals');
        
        /* THUMNAIL */
        $uploadedThumbnail = $request->getUploadedFiles();
        $thumbnail = $uploadedThumbnail['picture'];
        if($thumbnail->getError() === UPLOAD_ERR_OK) {
            $thumbnail_name = Core::moveUploadedFile($directory, $thumbnail);
            HospitalMeta::add_meta($hospital->id, 'thumbnail', $thumbnail_name);
        }

        /* GALLERY */
        $gallery_image_name = [];
        $uploadGallery = $request->getUploadedFiles();
        foreach ($uploadGallery['gallery'] as $picture) {
        if ($picture->getError() === UPLOAD_ERR_OK) {
                $picture_name = Core::moveUploadedFile($directory, $picture);
                $gallery_image_name[] = $picture_name;
            }
        }
        if( !empty($gallery_image_name) ) {
            $gallery_image_name_in_json = json_encode($gallery_image_name);
            HospitalMeta::add_meta($hospital->id, 'gallery', $gallery_image_name_in_json);
        }

        $this->flash->addMessage('success', 'Hospital has been added.');
        return $response->withRedirect($this->router->pathFor('hospitals.list'));

    }

    public function getUpdate($request, $response, $args)
    {

        $hospital = Hospital::find($args['id']);
        if( $hospital == null ) {
            return $this->view->render($response, 'templates/control-panel/admin/404.twig');
        }
        return $this->view->render($response, 'templates/control-panel/templates/admin/edit-hospital.twig', ['hospital' => $hospital, 'specialities' => Speciality::get()]);

    }

    public function update($request, $response, $args)
    {

        $hospital_id = intval($args['id']);
        $hospital    = Hospital::find($hospital_id);
        if( $hospital == null ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }

        $validation = $this->validator->validate($request, [
            'name'      => v::notEmpty(),
            'type'      => v::noWhitespace()->notEmpty()->alpha(),
            'phone'     => v::noWhitespace()->notEmpty()->phone(),
            'price'     => v::noWhitespace()->notEmpty(),
            'city'      => v::notEmpty(),
            'state'     => v::notEmpty(),
            'country'   => v::notEmpty(),
            'address'   => v::notEmpty(),
            'zip_code'  => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('create.new.hospital'));
        }

        if( $request->getParam('email') != $hospital->email ) {
            $validation = $this->validator->validate($request, [
                'email' => v::emailAvailable()
            ]);
            if( $validation->failed() ) {
                $this->flash->addMessage('error', 'Please enter the data correctly.');
                return $response->withRedirect($this->router->pathFor('hospital.update',['id' => $hospital->id]));
            }
        }

        $hospital->name = $request->getParam('name');
        $hospital->type  = $request->getParam('type');
        $hospital->email      = $request->getParam('email');
        if( !empty($request->getParam('password'))) {
            $hospital->password   = password_hash($request->getParam('password'), PASSWORD_DEFAULT);
        }
        $hospital->phone      = $request->getParam('phone');
        $hospital->price     = $request->getParam('price');
        $hospital->city       = $request->getParam('city');
        $hospital->state      = $request->getParam('state');
        $hospital->country    = $request->getParam('country');
        $hospital->address    = $request->getParam('address');
        $hospital->zip_code    = $request->getParam('zip_code');
        $hospital->about      = $request->getParam('about');
        $hospital->save();

        $directory =  base_path('public/uploads/hospitals');
        
        /* THUMNAIL */
        $uploadedThumbnail = $request->getUploadedFiles();
        $thumbnail = $uploadedThumbnail['picture'];
        if($thumbnail->getError() === UPLOAD_ERR_OK) {
            $thumbnail_name = Core::moveUploadedFile($directory, $thumbnail);
            HospitalMeta::update_meta($hospital->id, 'thumbnail', $thumbnail_name);
        }

        /* GALLERY */
        $gallery_image_name = [];
        $uploadGallery = $request->getUploadedFiles();
        foreach ($uploadGallery['gallery'] as $picture) {
        if ($picture->getError() === UPLOAD_ERR_OK) {
                $picture_name = Core::moveUploadedFile($directory, $picture);
                $gallery_image_name[] = $picture_name;
            }
        }
        if( !empty($gallery_image_name) ) {
            $gallery_image_name_in_json = json_encode($gallery_image_name);
            HospitalMeta::update_meta($hospital->id, 'gallery', $gallery_image_name_in_json);
        }

        $get_specialities = SpecialityRelationship::where('object_id', $hospital->id)->where('object_type', 'hospital');
        if( !empty($request->getParam('specialities')) ) {
            $get_specialities->delete();
            $specialities = [];
            foreach( $request->getParam('specialities') as $speciality ) {
                $sp = explode(',',$speciality);
                $specialities[] = [ 'speciality_id' => $sp[0], 'speciality_name' => $sp[1], 'object_id' => $hospital->id, 'object_type' => 'hospital'];
            }
            $insert_speciality = SpecialityRelationship::insert($specialities);
        } else {
            $get_specialities->delete();
        }

        if( !empty($request->getParam('amenties')) ) {
            $amenties = $request->getParam('amenties');
            $amenties_in_json = json_encode($amenties);
            HospitalMeta::update_meta($hospital->id, 'amenties', $amenties_in_json);
        }

        if( !empty($request->getParam('award_name')) ) {
            $awards = [];
            $count = count($request->getParam('award_name'));
            for( $i = 0; $i < $count; $i++ ) {
                $awards[] = [ 'award_name' => $request->getParam('award_name')[$i], 'award_year' => $request->getParam('award_year')[$i] ];
            }
            $awards_in_json = json_encode($awards);
            HospitalMeta::update_meta($hospital->id, 'awards', $awards_in_json);
        }

        $this->flash->addMessage('success', 'Hospital has been updated.');
        return $response->withRedirect($this->router->pathFor('hospital.update',['id' => $hospital->id]));

    }

    public function delete($request, $response, $args)
    {

        $hospital_id  = intval($args['id']);
        $hospital     = Hospital::find($hospital_id);
        if( $hospital == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $hospital_meta = HospitalMeta::where('hospital_id', $hospital_id);
        $specialities = SpecialityRelationship::where('object_id', $hospital_id)->where('object_type', 'hospital');

        $hospital->delete();
        $hospital_meta->delete();
        $specialities->delete();

        $this->flash->addMessage('success', 'Hospital has been deleted.');
        return $response->withRedirect($this->router->pathFor('hospitals.list'));

    }

    public function show($request, $response, $args)
    {

        $count          = Hospital::count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $hospitals      = Hospital::skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/hospitals/hospitals.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'hospitals' => $hospitals,
        ]);

    }

    public function single($request, $response, $args)
    {

        $hospital    = Hospital::find($args['id']);
        if( $hospital == null ) {
            return $this->view->render($response, 'templates/404.twig');
        }
        $doctors = User::where('hospital_id', $hospital->id)->get();
        $comments = Comment::where('object_type', 'hospital')->where('object_id', $hospital->id)->orderBy('created_at', 'desc')->get();
        return $this->view->render($response, 'templates/hospitals/single.twig',['hospital' => $hospital, 'doctors' => $doctors, 'comments' => $comments]);

    }

    public function sendEnquiry($request, $response, $args)
    {

        $hospital_id  = intval($args['id']);
        $hospital     = Hospital::find($hospital_id);
        if( $hospital == null )
            return $this->view->render($response, 'templates/404.twig');

        $validation = $this->validator->validate($request, [
            'name'        => v::notEmpty(),
            'phone'       => v::noWhitespace()->notEmpty()->phone(),
            'email'       => v::noWhitespace()->notEmpty()->email(),
            'procedure'   => v::notEmpty(),
            'travel_date' => v::notEmpty(),
            'message'     => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('hospitals.single',['id' => $hospital->id]));
        }

        $patient_id = ($_SESSION['user'] != '') ? $_SESSION['user'] : null;

        $enquiry = Enquiry::create([
            'name'        => $request->getParam('name'),
            'phone'       => $request->getParam('phone'),
            'email'       => $request->getParam('email'),
            'procedure'   => $request->getParam('procedure'),
            'travel_date' => $request->getParam('travel_date'),
            'message'     => $request->getParam('message'),
            'hospital_id' => $hospital->id,
            'patient_id'  => $patient_id
        ]);

        $this->flash->addMessage('success', 'Enquiry has been sent.');
        $_SESSION['old'] = '';
        return $response->withRedirect($this->router->pathFor('hospitals.single',['id' => $hospital->id]));

    }

    public function speciality($request, $response, $args)
    {

        $speciality_id = $args['id'];
        $speciality = Speciality::find($speciality_id);
        if( !$speciality )
            return $this->view->render($response, 'templates/404.twig');
        
        $ids = [];
        $sps = SpecialityRelationship::where('object_type', 'hospital')->where('speciality_id', $speciality_id)->get();
        if( $sps ) {
            foreach( $sps as $sp ) {
                $ids[] = $sp->object_id;
            } 
        }

        $count          = Hospital::whereIn('id', $ids)->count();   // Count of all available hospitals 
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $hospitals      = Hospital::whereIn('id', $ids)->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/hospitals/speciality.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'hospitals'  => $hospitals,
          'speciality' => $speciality
        ]);

    }

    public function search($request, $response, $args)
    {

        $query = $request->getParam('s');

        $hospital_name = (!empty($request->getParam('hospital_name'))) ? $request->getParam('hospital_name') : '';
        $speiclaities  = (!empty($request->getParam('speciality'))) ? $request->getParam('speciality') : [];
        $countries     = (!empty($request->getParam('country'))) ? $request->getParam('country') : [];

        
        $hospitals = DB::table('hospitals');
        if (!empty($hospital_name))
            $hospitals = $hospitals->where('name','like',"%$hospital_name%");

        if (!empty($countries))
            $hospitals = $hospitals->whereIn('country',$countries);

        if (!empty($speiclaities)) {
            $ids = [];
            $sps = SpecialityRelationship::where('object_type', 'hospital')->whereIn('speciality_id', $speiclaities)->get();
            if( $sps ) {
                foreach( $sps as $sp ) {
                    $ids[] = $sp->object_id;
                } 
            }
            $hospitals = $hospitals->whereIn('id',$ids);
        }

        $count = $hospitals->count();
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $hospitals      = $hospitals->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/hospitals/search.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'hospitals' => $hospitals,
        ]);

    }

    public function addComment($request, $response, $args)
    {

        $hospital_id = intval($args['id']);
        $patient   = User::find(intval($_SESSION['user']));
        $hospital    = Hospital::find($hospital_id);
        if( $hospital == null || $patient == null )
            return $this->view->render($response, 'templates/404.twig');

        $validation = $this->validator->validate($request, [
            'review_message'  => v::notEmpty(),
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('hospitals.single',['id' => $hospital->id]));
        }

        $comment = Comment::create([
            'object_id'    => $hospital->id,
            'object_type'  => 'hospital',
            'author_id'    => $patient->id,
            'author_name'  => $patient->full_name,
            'author_email' => $patient->email,
            //'author_ip'    => $_SERVER['REMOTE_ADDR'],
            'content'      => $request->getParam('review_message'),
            'rate'         => $request->getParam('review_rate')
        ]);

        $this->flash->addMessage('success', 'Review has been added.');
        $_SESSION['old'] = '';
        return $response->withRedirect($this->router->pathFor('hospitals.single',['id' => $hospital->id]));

    }

}