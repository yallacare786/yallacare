<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Post;
use App\Models\PostMeta;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Role;
use App\Models\Category;
use App\Models\CategoryRelationship;
use Respect\Validation\Validator as v;

class ArticleController extends Controller 
{

    public function list($request, $response, $args)
    {

        $count          = Post::where('post_type', 'post')->count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $articles       = Post::where('post_type', 'post')->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/control-panel/templates/admin/articles.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'articles'       => $articles,
        ]);

    }

    public function getCreate($request, $response, $args)
    {
        $categories = Category::get();
        return $this->view->render($response, 'templates/control-panel/templates/admin/new-article.twig',['categories' => $categories]);
    }

    public function create($request, $response, $args)
    {

        $article = Post::create([
            'title'      => $request->getParam('title'),
            'content'    => $request->getParam('content'),
            'status'     => $request->getParam('status'),
            'post_type'  => 'post',
        ]);

        if( !empty($request->getParam('categories')) ) {
            $categories = [];
            foreach( $request->getParam('categories') as $category ) {
                $cat = explode(',',$category);
                $categories[] = [ 'category_id' => $cat[0], 'category_name' => $cat[1], 'object_id' => $article->id];
            }
            $insert_category = CategoryRelationship::insert($categories);
        }

        $directory =  base_path('public/uploads/posts');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['picture'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            PostMeta::add_meta($article->id, 'thumbnail', $filename);
        }

        $this->flash->addMessage('success', 'Article has been added.');
        return $response->withRedirect($this->router->pathFor('articles.list'));

    }

    public function getUpdate($request, $response, $args)
    {

        $article    = Post::find($args['id']);
        $categories = Category::get();
        if( $article == null ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }

        return $this->view->render($response, 'templates/control-panel/templates/admin/edit-article.twig',['article' => $article, 'categories' => $categories]);

    }

    public function update($request, $response, $args)
    {

        $article_id = intval($args['id']);
        $article    = Post::find($article_id);
        if( $article == null )
            return $this->view->render($response, 'templates/control-panel/admin/404.twig');

        $get_categories = CategoryRelationship::where('object_id', $article->id);
        if( !empty($request->getParam('categories')) ) {
            $get_categories->delete();
            $categories = [];
            foreach( $request->getParam('categories') as $category ) {
                $cat = explode(',',$category);
                $categories[] = [ 'category_id' => $cat[0], 'category_name' => $cat[1], 'object_id' => $article->id];
            }
            $insert_category = CategoryRelationship::insert($categories);
        } else {
            $get_categories->delete();
        }

        $article->title    = $request->getParam('title');
        $article->content  = $request->getParam('content');
        $article->save();

        $directory = base_path('public/uploads/posts');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['picture'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            PostMeta::update_meta($post->id, 'thumbnail', $filename);
        }

        $this->flash->addMessage('success', 'Article has been updated.');
        return $response->withRedirect($this->router->pathFor('article.update',['id' => $article_id]));

    }

    public function delete($request, $response, $args)
    {

        $article_id      = intval($args['id']);
        $article         = Post::find($article_id);
        if( $article == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $article_meta = PostMeta::where('post_id', $article->id);
        $categories   = CategoryRelationship::where('object_id', $article_id);

        $article->delete();
        $article_meta->delete();
        $categories->delete();

        $this->flash->addMessage('success', 'Article has been deleted.');
        return $response->withRedirect($this->router->pathFor('articles.list'));

    }

}