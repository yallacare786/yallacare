<?php 

namespace App\Controllers;

use App\Core;
use Respect\Validation\Validator as v;

class HomeController extends Controller 
{

    public function show($request, $response, $args)
    {

        return $this->view->render($response, 'templates/home.twig');

    }

}