<?php 

namespace App\Controllers;

use App\Core;
use App\Models\User;
use App\Models\Role;
use Respect\Validation\Validator as v;

class AdminController extends Controller 
{

    public function show($request, $response, $args)
    {
        return $this->view->render($response, 'templates/control-panel/templates/admin/index.twig');
    }

    public function getSignIn($request, $response, $args)
    {
        return $this->view->render($response, 'templates/control-panel/templates/admin/signin.twig');
    }

    public function signIn($request, $response, $args)
    {

        $validation = $this->validator->validate($request, [
            'email'         => v::noWhitespace()->notEmpty()->email(),
            'password'      => v::notEmpty(),
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('admin.signin'));
        }

        $auth = $this->auth->admin_attempt($request->getParam('email'),$request->getParam('password'));
        if( !$auth ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('admin.signin'));
        }
        
        return $response->withRedirect($this->router->pathFor('admin.index'));

    }

}