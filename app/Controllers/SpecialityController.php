<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Speciality;
use Respect\Validation\Validator as v;

class SpecialityController extends Controller 
{

    public function list($request, $response, $args)
    {
        $count          = Speciality::count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Users on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $specialities     = Speciality::skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/control-panel/templates/admin/specialities.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'specialities' => $specialities ,
        ]);
    }

    public function create($request, $response, $args)
    {
        $validation = $this->validator->validate($request, [
            'name' => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('specialities.list'));
        }
        
        $speciality = Speciality::create([
            'name' => $request->getParam('name'),
        ]);

        $this->flash->addMessage('success', 'Speciality has been added.');
        return $response->withRedirect($this->router->pathFor('specialities.list'));
    }

    public function getUpdate($request, $response, $args)
    {
        $speciality = Speciality::find($args['id']);
        if( $speciality == null ) {
            return $this->view->render($response, 'templates/control-panel/admin/404.twig');
        }
        return $this->view->render($response, 'templates/control-panel/templates/admin/edit-speciality.twig', ['speciality' => $speciality]);
    }

    public function update($request, $response, $args)
    {
        $speciality_id = intval($args['id']);
        $speciality    = Speciality::find($speciality_id);
        if( $speciality == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $validation = $this->validator->validate($request, [
            'name'    => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('speciality.update',['id' => $speciality_id]));
        }

        $speciality->name = $request->getParam('name');
        $speciality->save();

        $this->flash->addMessage('success', 'Speciality has been updated.');
        return $response->withRedirect($this->router->pathFor('speciality.update',['id' => $speciality_id]));
    }
        
    public function delete($request, $response, $args)
    {
        $speciality_id = intval($args['id']);
        $speciality    = Speciality::find($speciality_id);
        if( $speciality == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $speciality->delete();
        $this->flash->addMessage('success', 'Speciality has been deleted.');
        return $response->withRedirect($this->router->pathFor('specialities.list'));
    }

}