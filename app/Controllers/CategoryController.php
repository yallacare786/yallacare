<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Category;
use Respect\Validation\Validator as v;

class CategoryController extends Controller 
{

    public function list($request, $response, $args)
    {
        $count          = Category::count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Categories on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $categories   = Category::skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/control-panel/templates/admin/categories.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'categories' => $categories ,
        ]);
    }

    public function create($request, $response, $args)
    {
        $validation = $this->validator->validate($request, [
            'name' => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('categories.list'));
        }
        
        $category = Category::create([
            'name' => $request->getParam('name'),
        ]);

        $this->flash->addMessage('success', 'Category has been added.');
        return $response->withRedirect($this->router->pathFor('categories.list'));
    }

    public function getUpdate($request, $response, $args)
    {
        $category = Category::find($args['id']);
        if( $category == null ) {
            return $this->view->render($response, 'templates/control-panel/admin/404.twig');
        }
        return $this->view->render($response, 'templates/control-panel/templates/admin/edit-category.twig', ['category' => $category]);
    }

    public function update($request, $response, $args)
    {
        $category_id = intval($args['id']);
        $category    = Category::find($category_id);
        if( $category == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $validation = $this->validator->validate($request, [
            'name'    => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('category.update',['id' => $category_id]));
        }

        $category->name = $request->getParam('name');
        $category->save();

        $this->flash->addMessage('success', 'Category has been updated.');
        return $response->withRedirect($this->router->pathFor('category.update',['id' => $category_id]));
    }
        
    public function delete($request, $response, $args)
    {
        $category_id = intval($args['id']);
        $category    = Category::find($category_id);
        if( $category == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $category->delete();
        $this->flash->addMessage('success', 'Category has been deleted.');
        return $response->withRedirect($this->router->pathFor('categories.list'));
    }

}