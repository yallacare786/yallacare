<?php 

namespace App\Controllers;

use App\Core;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Hospital;
use App\Models\HospitalMeta;
use App\Models\Role;
use App\Models\Speciality;
use App\Models\SpecialityRelationship;
use App\Models\Appointment;
use App\Models\Comment;
use Respect\Validation\Validator as v;
use Illuminate\Database\Capsule\Manager as DB;

class DoctorController extends Controller
{

    public function show($request, $response, $args)
    {

        $doctor_role_id = Role::get_doctor_role();
        $count          = User::where('role_id', $doctor_role_id)->count();   // Count of all available doctors      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Doctors on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $doctors        = User::where('role_id', $doctor_role_id)->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/doctors/doctors.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'doctors' => $doctors,
        ]);

    }

    public function single($request, $response, $args)
    {
        $doctor_role_id = Role::get_doctor_role();
        $doctor    = User::find($args['id']);
        if( $doctor == null || $doctor->role_id != $doctor_role_id ) {
            return $this->view->render($response, 'templates/404.twig');
        }
        $comments = Comment::where('object_type', 'doctor')->where('object_id', $doctor->id)->orderBy('created_at', 'desc')->get();
        return $this->view->render($response, 'templates/doctors/single.twig',['doctor' => $doctor, 'comments' => $comments]);

    }

    public function book($request, $response, $args)
    {

        $doctor_role_id = Role::get_doctor_role();
        $doctor_id      = intval($args['id']);
        $patient_id     = intval($_SESSION['user']);
        $doctor         = User::find($doctor_id);
        if( $doctor == null )
            return $this->view->render($response, 'templates/404.twig');

        $validation = $this->validator->validate($request, [
            'date_of_booking'  => v::notEmpty(),
            'time_of_booking'  => v::notEmpty(),
            'book_message'     => v::notEmpty()
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('doctors.single',['id' => $doctor->id]));
        }

        $appointment = Appointment::create([
            'patient_id'    => $patient_id,
            'doctor_id'     => $doctor_id,
            'book_date'     => $request->getParam('date_of_booking'),
            'book_time'     => $request->getParam('time_of_booking'),
            'message'       => $request->getParam('book_message')
        ]);

        $this->flash->addMessage('success', 'Book has been sent.');
        $_SESSION['old'] = '';
        return $response->withRedirect($this->router->pathFor('doctors.single',['id' => $doctor->id]));

    }

    public function speciality($request, $response, $args)
    {

        $speciality_id = $args['id'];
        $speciality = Speciality::find($speciality_id);
        if( !$speciality )
            return $this->view->render($response, 'templates/404.twig');
        
        $ids = [];
        $sps = SpecialityRelationship::where('object_type', 'hospital')->where('speciality_id', $speciality_id)->get();
        if( $sps ) {
            foreach( $sps as $sp ) {
                $ids[] = $sp->object_id;
            } 
        }

        $count          = Hospital::whereIn('id', $ids)->count();   // Count of all available hospitals 
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $hospitals      = Hospital::whereIn('id', $ids)->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/hospitals/speciality.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'hospitals'  => $hospitals,
          'speciality' => $speciality
        ]);

    }

    public function search($request, $response, $args)
    {
        $doctor_role_id = Role::get_doctor_role();
        $doctor_name    = (!empty($request->getParam('doctor_name'))) ? $request->getParam('doctor_name') : '';
        $speiclaities   = (!empty($request->getParam('speciality'))) ? $request->getParam('speciality') : [];
        $countries      = (!empty($request->getParam('country'))) ? $request->getParam('country') : [];

        $doctors = DB::table('users')->where('role_id', $doctor_role_id);
        if (!empty($doctor_name))
            $doctors = $doctors->where('full_name','like',"%$doctor_name%");

        if (!empty($countries))
            $doctors = $doctors->whereIn('country',$countries);

        if (!empty($speiclaities)) {
            $ids = [];
            $sps = SpecialityRelationship::where('object_type', 'doctor')->whereIn('speciality_id', $speiclaities)->get();
            if( $sps ) {
                foreach( $sps as $sp ) {
                    $ids[] = $sp->object_id;
                } 
            }
            $doctors = $doctors->whereIn('id',$ids);
        }

        $count          = $doctors->count();
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $doctors        = $doctors->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/doctors/search.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'doctors' => $doctors,
        ]);

    }

    public function addComment($request, $response, $args)
    {

        $doctor_id = intval($args['id']);
        $patient   = User::find(intval($_SESSION['user']));
        $doctor    = User::find($doctor_id);
        if( $doctor == null || $patient == null )
            return $this->view->render($response, 'templates/404.twig');

        $validation = $this->validator->validate($request, [
            'review_message'  => v::notEmpty(),
        ]);
        
        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('doctors.single',['id' => $doctor->id]));
        }

        $comment = Comment::create([
            'object_id'    => $doctor->id,
            'object_type'  => 'doctor',
            'author_id'    => $patient->id,
            'author_name'  => $patient->full_name,
            'author_email' => $patient->email,
            //'author_ip'    => $_SERVER['REMOTE_ADDR'],
            'content'      => $request->getParam('review_message'),
            'rate'         => $request->getParam('review_rate')
        ]);

        $this->flash->addMessage('success', 'Review has been added.');
        $_SESSION['old'] = '';
        return $response->withRedirect($this->router->pathFor('doctors.single',['id' => $doctor->id]));

    }

}