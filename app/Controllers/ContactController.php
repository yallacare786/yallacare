<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Message;
use Respect\Validation\Validator as v;

class ContactController extends Controller 
{

    public function show($request, $response, $args)
    {
        return $this->view->render($response, 'templates/contact.twig');
    }

    public function send($request, $response, $args)
    {

        $validation = $this->validator->validate($request, [
            'name'    => v::notEmpty(),
            'email'   => v::noWhitespace()->notEmpty()->email(),
            'phone'   => v::notEmpty()->phone(),
            'subject' => v::notEmpty(),
            'message' => v::notEmpty()
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('contact'));
        }

        $message = Message::create([
            'name'    => $request->getParam('name'),
            'email'   => $request->getParam('email'),
            'phone'   => $request->getParam('phone'),
            'subject' => $request->getParam('subject'),
            'message' => $request->getParam('message')
        ]);

        $this->flash->addMessage('success', 'Message has been sent.');
        $_SESSION['old'] = '';
        return $response->withRedirect($this->router->pathFor('contact'));
    }

}