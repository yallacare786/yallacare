<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Appointment;
use App\Models\User;
use Respect\Validation\Validator as v;

class AppointmentController extends Controller 
{

    public function list($request, $response, $args)
    {
        $count          = Appointment::count();
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Users on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));
        $skip           = ($page - 1) * $limit;
        $appointments   = Appointment::skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/control-panel/templates/admin/appointments.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'appointments' => $appointments ,
        ]);
    }

    public function update($request, $response, $args)
    {
        return $this->view->render($response, 'templates/contact.twig');
    }

    public function delete($request, $response, $args)
    {
        return $this->view->render($response, 'templates/contact.twig');
    }

    

}