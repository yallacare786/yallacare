<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Enquiry;
use App\Models\EnquiryMeta;
use Respect\Validation\Validator as v;

class SecondOpinionController extends Controller 
{

    public function show($request, $response, $args)
    {
        return $this->view->render($response, 'templates/second-opinion.twig');
    }

    public function send($request, $response, $args)
    {

        $validation = $this->validator->validate($request, [
            'name'    => v::notEmpty(),
            'email'   => v::noWhitespace()->notEmpty()->email(),
            'phone'   => v::notEmpty()->phone(),
            'message' => v::notEmpty()
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('second.opinion'));
        }

        $enquiry = Enquiry::create([
            'name'    => $request->getParam('name'),
            'email'   => $request->getParam('email'),
            'phone'   => $request->getParam('phone'),
            'message' => $request->getParam('message')
        ]);

        $directory =  base_path('public/uploads/enquiries');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['report'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            EnquiryMeta::add_meta($enquiry->id, 'report', $filename);
        }

        $this->flash->addMessage('success', 'Enquiry sent successfully.');
        $_SESSION['old'] = '';
        return $response->withRedirect($this->router->pathFor('second.opinion'));
    }

}