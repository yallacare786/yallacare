<?php 

namespace App\Controllers;

use App\Core;
use App\Models\User;
use App\Models\Role;
use Respect\Validation\Validator as v;

class HowController extends Controller 
{

    public function show($request, $response, $args)
    {
        return $this->view->render($response, 'templates/how.twig');
    }

}