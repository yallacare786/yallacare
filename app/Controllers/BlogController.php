<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Post;
use App\Models\PostMeta;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Role;
use App\Models\Category;
use App\Models\CategoryRelationship;
use Respect\Validation\Validator as v;

class BlogController extends Controller 
{

    public function list($request, $response, $args)
    {

        $count          = Post::where('post_type', 'post')->count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $articles       = Post::where('post_type', 'post')->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/blog/blog.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'articles'       => $articles,
        ]);

    }

    public function single($request, $response, $args)
    {

        $article    = Post::find($args['id']);
        if( $article == null ) {
            return $this->view->render($response, 'templates/404.twig');
        }

        return $this->view->render($response, 'templates/blog/single.twig',['article' => $article]);

    }

    public function category($request, $response, $args)
    {

        $category_id = $args['id'];
        $category = Category::find($category_id);
        if( !$category )
            return $this->view->render($response, 'templates/404.twig');
        
        $ids = [];
        $cats = CategoryRelationship::where('category_id', $category_id)->get();
        if( $cats ) {
            foreach( $cats as $cat ) {
                $ids[] = $cat->object_id;
            } 
        }

        $count          = Post::where('post_type', 'post')->whereIn('id', $ids)->count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $articles       = Post::where('post_type', 'post')->whereIn('id', $ids)->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/blog/category.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'articles' => $articles,
          'category' => $category
        ]);

    }

    public function search($request, $response, $args)
    {

        $query = $request->getParam('s');

        $count          = Post::where('post_type', 'post')->where('title','like',"%$query%")->count();   // Count of all available users
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $articles       = Post::where('post_type', 'post')->where('title','like',"%$query%")->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/blog/search.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'articles' => $articles,
          'query'    => $query
        ]);

    }

}