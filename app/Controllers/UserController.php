<?php 

namespace App\Controllers;

use App\Core;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Role;
use App\Models\Speciality;
use App\Models\SpecialityRelationship;
use Respect\Validation\Validator as v;

class UserController extends Controller 
{

    public function list($request, $response, $args)
    {

        $type = $args['type'];
        if( $type == 'patients' ) {
            $role_id = Role::get_patient_role();
            $role_name = 'patient';
        } else if ( $type == 'doctors' ) {
            $role_id = Role::get_doctor_role();
            $role_name = 'doctor';
        } else if ( $type == 'admins' ) {
            $role_id = Role::get_admin_role();
            $role_name = 'admin';
        } else {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }

        $count          = User::where('role_id', $role_id)->count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Users on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $users          = User::where('role_id', $role_id)->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/control-panel/templates/admin/users.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'users'       => $users,
          'page_title'  => ucwords($args['type'] . ' list'),
          'role_name'   => $role_name
        ]);

    }

    public function getCreate($request, $response, $args)
    {
        $specialities = Speciality::get();
        if( $args['type'] == 'doctor' ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/new-doctor.twig',['role_name' => $args['type'], 'specialities' => $specialities]);
        } else if( $args['type'] == 'patient' || $args['type'] == 'admin' ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/new-user.twig',['role_name' => $args['type']]);
        } else {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }
    }

    public function create($request, $response, $args)
    {

        $validation = $this->validator->validate($request, [
            'full_name'     => v::notEmpty(),
            'email'         => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'password'      => v::notEmpty(),
            'phone'         => v::noWhitespace()->notEmpty()->phone(),
            'birth'         => v::noWhitespace()->notEmpty()->date('d-m-Y'),
            'city'          => v::notEmpty(),
            'state'         => v::notEmpty(),
            'country'       => v::notEmpty(),
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('create.new.user',['type' => $args['type']]));
        }

        $role = Role::get_role_by_name($args['type']);

        $user = User::create([
            'full_name' => $request->getParam('full_name'),
            'email'      => $request->getParam('email'),
            'password'   => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
            'phone'      => $request->getParam('phone'),
            'gender'     => $request->getParam('gender'),
            'birth'      => $request->getParam('birth'),
            'city'       => $request->getParam('city'),
            'state'      => $request->getParam('state'),
            'country'    => $request->getParam('country'),
            'about'      => $request->getParam('about'),
            'role_id'    => $role->id
        ]);

        if( $args['type'] == 'doctor' ) {

            if( !empty($request->getParam('specialities')) ) {
                $specialities = [];
                foreach( $request->getParam('specialities') as $speciality ) {
                    $sp = explode(',',$speciality);
                    $specialities[] = [ 'speciality_id' => $sp[0], 'speciality_name' => $sp[1], 'object_id' => $user->id, 'object_type' => 'doctor'];
                }
                $insert_speciality = SpecialityRelationship::insert($specialities);
            }

            if( !empty($request->getParam('qualification_name')) ) {
                $qualifications = [];
                $count = count($request->getParam('qualification_name'));
                for( $i = 0; $i < $count; $i++ ) {
                    $qualifications[] = [ 'qualification_name' => $request->getParam('qualification_name')[$i], 'qualification_year' => $request->getParam('qualification_year')[$i] ];
                }
                $qualifications_in_json = json_encode($qualifications);
                UserMeta::add_meta($user->id, 'qualifications', $qualifications_in_json);
            }

            if( !empty($request->getParam('service_name')) ) {
                $services = [];
                $count = count($request->getParam('service_name'));
                for( $i = 0; $i < $count; $i++ ) {
                    $services[] = [ 'service_name' => $request->getParam('service_name')[$i], 'service_experience' => $request->getParam('service_experience')[$i], 'service_price' => $request->getParam('service_price')[$i] ];
                }
                $services_in_json = json_encode($services);
                UserMeta::add_meta($user->id, 'services', $services_in_json);
            }

            if( !empty($request->getParam('insurance')) ) {
                $insurances = [];
                $count = count($request->getParam('insurance'));
                for( $i = 0; $i < $count; $i++ ) {
                    $insurances[] = [ 'insurance' => $request->getParam('insurance')[$i] ];
                }
                $insurances_in_json = json_encode($insurances);
                UserMeta::add_meta($user->id, 'insurances', $insurances_in_json);
            }

            $working_hours = [
                'monday' => [
                    'start'     => $request->getParam('monday_start'),
                    'end'       => $request->getParam('monday_end')
                ],
                'tuesday' => [
                    'start'     => $request->getParam('tuesday_start'),
                    'end'       => $request->getParam('tuesday_end')
                ],
                'wednesday' => [
                    'start'     => $request->getParam('wednesday_start'),
                    'end'       => $request->getParam('wednesday_end')
                ],
                'thursday' => [
                    'start'     => $request->getParam('thursday_start'),
                    'end'       => $request->getParam('thursday_end')
                ],
                'friday' => [
                    'start'     => $request->getParam('friday_start'),
                    'end'       => $request->getParam('friday_end')
                ],
                'saturday' => [
                    'start'     => $request->getParam('saturday_start'),
                    'end'       => $request->getParam('saturday_end')
                ],
                'sunday' => [
                    'start'     => $request->getParam('sunday_start'),
                    'end'       => $request->getParam('sunday_end')
                ]
            ];
            $working_hours_in_json = json_encode($working_hours);
            UserMeta::add_meta($user->id, 'working_hours', $working_hours_in_json);

        }

        $directory =  base_path('public/uploads/users');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['picture'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            UserMeta::add_meta($user->id, 'avatar', $filename);
        }

        $this->flash->addMessage('success', 'User has been added.');
        return $response->withRedirect($this->router->pathFor('users.list',['type' => $args['type'] . 's']));

    }

    public function getUpdate($request, $response, $args)
    {

        $user = User::find($args['id']);
        if( $user == null ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }

        if( Role::get_role_name_by_id($user->role_id) == 'doctor' ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/edit-doctor.twig',
                [
                    'id'            => $user->id,
                    'user'          => $user,
                    'role_name'     => Role::get_role_name_by_id($user->role_id),
                    'specialities'  => Speciality::get()
                ]
        );
        } else if ( Role::get_role_name_by_id($user->role_id) == 'patient' || Role::get_role_name_by_id($user->role_id) == 'admin' ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/edit-user.twig',
                [
                    'id'        => $user->id,
                    'user'      => $user,
                    'role_name' => Role::get_role_name_by_id($user->role_id)
                ]
            );
        } else {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }

    }

    public function update($request, $response, $args)
    {

        $user_id = intval($args['id']);
        $user    = User::find($user_id);
        if( $user == null ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }

        $validation = $this->validator->validate($request, [
            'full_name'     => v::notEmpty(),
            'phone'         => v::noWhitespace()->notEmpty()->phone(),
            'birth'         => v::noWhitespace()->notEmpty()->date('d-m-Y'),
            'city'          => v::notEmpty(),
            'state'         => v::notEmpty(),
            'country'       => v::notEmpty(),
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('user.update',['id' => $user->id]));
        }

        if( $request->getParam('email') != $user->email ) {
            $validation = $this->validator->validate($request, [
                'email' => v::emailAvailable()
            ]);
            if( $validation->failed() ) {
                $this->flash->addMessage('error', 'Please enter the data correctly.');
                return $response->withRedirect($this->router->pathFor('user.update',['id' => $user->id]));
            }
        }

        $user->full_name = $request->getParam('full_name');
        $user->email      = $request->getParam('email');
        if( !empty($request->getParam('password'))) {
            $user->password   = password_hash($request->getParam('password'), PASSWORD_DEFAULT);
        }
        $user->phone      = $request->getParam('phone');
        $user->gender     = $request->getParam('gender');
        $user->birth      = $request->getParam('birth');
        $user->city       = $request->getParam('city');
        $user->state      = $request->getParam('state');
        $user->country    = $request->getParam('country');
        $user->about      = $request->getParam('about');
        $user->save();

        $directory =  base_path('public/uploads/users');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile  = $uploadedFiles['picture'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            UserMeta::update_meta($user->id, 'avatar', $filename);
        }

        if( Role::get_role_name_by_id($user->role_id) == 'doctor' ) {

            $get_specialities = SpecialityRelationship::where('object_id', $user->id)->where('object_type', 'doctor');
            if( !empty($request->getParam('specialities')) ) {
                $get_specialities->delete();
                $specialities = [];
                foreach( $request->getParam('specialities') as $speciality ) {
                    $sp = explode(',',$speciality);
                    $specialities[] = [ 'speciality_id' => $sp[0], 'speciality_name' => $sp[1], 'object_id' => $user->id, 'object_type' => 'doctor'];
                }
                $insert_speciality = SpecialityRelationship::insert($specialities);
            } else {
                $get_specialities->delete();
            }

            if( !empty($request->getParam('qualification_name')) ) {
                $qualifications = [];
                $count = count($request->getParam('qualification_name'));
                for( $i = 0; $i < $count; $i++ ) {
                    $qualifications[] = [ 'qualification_name' => $request->getParam('qualification_name')[$i], 'qualification_year' => $request->getParam('qualification_year')[$i] ];
                }
                $qualifications_in_json = json_encode($qualifications);
                UserMeta::update_meta($user->id, 'qualifications', $qualifications_in_json);
            }

            if( !empty($request->getParam('service_name')) ) {
                $services = [];
                $count = count($request->getParam('service_name'));
                for( $i = 0; $i < $count; $i++ ) {
                    $services[] = [ 'service_name' => $request->getParam('service_name')[$i], 'service_experience' => $request->getParam('service_experience')[$i], 'service_price' => $request->getParam('service_price')[$i] ];
                }
                $services_in_json = json_encode($services);
                UserMeta::update_meta($user->id, 'services', $services_in_json);
            }

            if( !empty($request->getParam('insurance')) ) {
                $insurances = [];
                $count = count($request->getParam('insurance'));
                for( $i = 0; $i < $count; $i++ ) {
                    $insurances[] = [ 'insurance' => $request->getParam('insurance')[$i] ];
                }
                $insurances_in_json = json_encode($insurances);
                UserMeta::update_meta($user->id, 'insurances', $insurances_in_json);
            }

            $working_hours = [
                'monday' => [
                    'start'     => $request->getParam('monday_start'),
                    'end'       => $request->getParam('monday_end')
                ],
                'tuesday' => [
                    'start'     => $request->getParam('tuesday_start'),
                    'end'       => $request->getParam('tuesday_end')
                ],
                'wednesday' => [
                    'start'     => $request->getParam('wednesday_start'),
                    'end'       => $request->getParam('wednesday_end')
                ],
                'thursday' => [
                    'start'     => $request->getParam('thursday_start'),
                    'end'       => $request->getParam('thursday_end')
                ],
                'friday' => [
                    'start'     => $request->getParam('friday_start'),
                    'end'       => $request->getParam('friday_end')
                ],
                'saturday' => [
                    'start'     => $request->getParam('saturday_start'),
                    'end'       => $request->getParam('saturday_end')
                ],
                'sunday' => [
                    'start'     => $request->getParam('sunday_start'),
                    'end'       => $request->getParam('sunday_end')
                ]
            ];
            $working_hours_in_json = json_encode($working_hours);
            UserMeta::update_meta($user->id, 'working_hours', $working_hours_in_json);

        }

        $this->flash->addMessage('success', 'User has been updated.');
        return $response->withRedirect($this->router->pathFor('user.update',['id' => $user->id]));

    }

    public function delete($request, $response, $args)
    {

        $user_id      = intval($args['id']);
        $user         = User::find($user_id);
        if( $user == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $role_name    = Role::get_role_name_by_id($user->role_id);
        $user_meta    = UserMeta::where('user_id', $user_id);
        $specialities = SpecialityRelationship::where('object_id', $user_id);

        $user->delete();
        $user_meta->delete();
        $specialities->delete();
        return $response->withRedirect($this->router->pathFor('users.list',['type' => $role_name . 's']));

    }

    public function patientProfile($request, $response, $args)
    {

        $patient = User::find($args['id']);
        if( $patient == null || $_SESSION['user'] != $patient->id ) {
            return $this->view->render($response, 'templates/404.twig');
        }
        return $this->view->render($response, 'templates/patients/profile.twig',['patient' => $patient]);

    }

    public function getEditPatient($request, $response, $args)
    {

        $patient = User::find($args['id']);
        if( $patient == null || $_SESSION['user'] != $patient->id ) {
            return $this->view->render($response, 'templates/404.twig');
        }
        return $this->view->render($response, 'templates/patients/edit.twig',['patient' => $patient]);

    }

    public function editPatient($request, $response, $args)
    {

        $patient_id = intval($args['id']);
        $patient    = User::find($patient_id);
        if( $patient == null || $_SESSION['user'] != $patient->id ) {
            return $this->view->render($response, 'templates/404.twig');
        }

        $validation = $this->validator->validate($request, [
            'full_name'     => v::notEmpty(),
            'phone'         => v::noWhitespace()->notEmpty()->phone(),
            'birth'         => v::noWhitespace()->notEmpty()->date('d-m-Y'),
            'city'          => v::notEmpty(),
            'state'         => v::notEmpty(),
            'country'       => v::notEmpty(),
        ]);

        if( $validation->failed() ) {
            $this->flash->addMessage('error', 'Please enter the data correctly.');
            return $response->withRedirect($this->router->pathFor('edit.patient',['id' => $patient->id]));
        }

        if( $request->getParam('email') != $patient->email ) {
            $validation = $this->validator->validate($request, [
                'email' => v::emailAvailable()
            ]);
            if( $validation->failed() ) {
                $this->flash->addMessage('error', 'Please enter the data correctly.');
                return $response->withRedirect($this->router->pathFor('edit.patient',['id' => $patient->id]));
            }
        }

        $patient->full_name = $request->getParam('full_name');
        $patient->email      = $request->getParam('email');
        if( !empty($request->getParam('password'))) {
            $patient->password   = password_hash($request->getParam('password'), PASSWORD_DEFAULT);
        }
        $patient->phone      = $request->getParam('phone');
        $patient->gender     = $request->getParam('gender');
        $patient->birth      = $request->getParam('birth');
        $patient->city       = $request->getParam('city');
        $patient->state      = $request->getParam('state');
        $patient->country    = $request->getParam('country');
        $patient->about      = $request->getParam('about');
        $patient->save();

        $directory =  base_path('public/uploads/users');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile  = $uploadedFiles['picture'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            UserMeta::update_meta($patient->id, 'avatar', $filename);
        }

        $this->flash->addMessage('success', 'Profile has been updated.');
        return $response->withRedirect($this->router->pathFor('edit.patient',['id' => $patient->id]));

    }

}