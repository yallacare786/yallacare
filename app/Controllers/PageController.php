<?php 

namespace App\Controllers;

use App\Core;
use App\Models\Post;
use App\Models\PostMeta;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Role;
use Respect\Validation\Validator as v;

class PageController extends Controller 
{

    public function list($request, $response, $args)
    {

        $count          = Post::where('post_type', 'page')->count();   // Count of all available users      
        $page           = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit          = 10; // Number of Articles on one page   
        $lastpage       = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));    // the number of the pages
        $skip           = ($page - 1) * $limit;
        $pages          = Post::where('post_type', 'page')->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get();

        return $this->view->render($response, 'templates/control-panel/templates/admin/pages.twig', [
            'pagination'    => [
                'needed'        => $count > $limit,
                'count'         => $count,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'prev'          => $page-1,
                'next'          => $page+1,
                'start'         => max(1, $page - 4),
                'end'           => min($page + 4, $lastpage),
            ],
          'pages'       => $pages,
        ]);

    }

    public function getCreate($request, $response, $args)
    {
        return $this->view->render($response, 'templates/control-panel/templates/admin/new-page.twig');
    }

    public function create($request, $response, $args)
    {

        $article = Post::create([
            'title'      => $request->getParam('title'),
            'content'    => $request->getParam('content'),
            'status'     => $request->getParam('status'),
            'post_type'  => 'page',
        ]);

        $directory =  base_path('public/uploads/posts');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['picture'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            PostMeta::add_meta($article->id, 'thumbnail', $filename);
        }

        $this->flash->addMessage('success', 'Page has been added.');
        return $response->withRedirect($this->router->pathFor('pages.list'));

    }

    public function getUpdate($request, $response, $args)
    {

        $page    = Post::find($args['id']);
        if( $page == null ) {
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');
        }

        return $this->view->render($response, 'templates/control-panel/templates/admin/edit-page.twig',['page' => $page]);

    }

    public function update($request, $response, $args)
    {

        $page_id = intval($args['id']);
        $page    = Post::find($page_id);
        if( $page == null )
            return $this->view->render($response, 'templates/control-panel/admin/404.twig');

        $page->title    = $request->getParam('title');
        $page->content  = $request->getParam('content');
        $page->save();

        $directory = base_path('public/uploads/posts');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['picture'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = Core::moveUploadedFile($directory, $uploadedFile);
            PostMeta::update_meta($page->id, 'thumbnail', $filename);
        }

        $this->flash->addMessage('success', 'Page has been updated.');
        return $response->withRedirect($this->router->pathFor('page.update',['id' => $page_id]));

    }

    public function delete($request, $response, $args)
    {

        $page_id      = intval($args['id']);
        $page         = Post::find($page_id);
        if( $page == null )
            return $this->view->render($response, 'templates/control-panel/templates/admin/404.twig');

        $page_meta = PostMeta::where('post_id', $page->id);

        $page->delete();
        $page_meta->delete();

        $this->flash->addMessage('success', 'Page has been deleted.');
        return $response->withRedirect($this->router->pathFor('pages.list'));

    }

}