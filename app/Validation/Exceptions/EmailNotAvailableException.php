<?php 

namespace App\Validation\Exceptions;
use Respect\Validation\Exceptions\ValidationException;

class EmailNotAvailableException extends ValidationException
{

    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Email does not exists.',
        ]
    ];

}