<?php

session_start();

date_default_timezone_set('Europe/Paris');
setlocale (LC_TIME, 'fr_FR.utf8','fra');

require __DIR__ . '/../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager;
use Respect\Validation\Validator as v;
use App\Models\Hospital;
use App\Models\Speciality;
use App\Models\SpecialityRelationship;
use App\Models\Category;
use App\Models\CategoryRelationship;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\PostMeta;
use App\Models\HospitalMeta;

if (!function_exists('base_path')) {
    function base_path($path = '') {
        return __DIR__ . '/..//' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

try {
    (new Dotenv\Dotenv(base_path()))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    die($e);
}

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'db' => [
            'driver'    => getenv('DB_DRIVER'),
            'host'      => getenv('DB_HOST'),
            'database'  => getenv('DB_NAME'),
            'username'  => getenv('DB_USER'),
            'password'  => getenv('DB_PASSWORD'),
            'charset'   => getenv('DB_CHARSET'),
            'collation' => getenv('DB_COLLATION'),
            'prefix'    => getenv('DB_PREFIX')
        ]
    ],
]);

$container = $app->getContainer();

$capsule = new Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) {
    return $capsule;
};

$container['flash'] = function ($container) {
    return new \Slim\Flash\Messages;
};

$container['csrf'] = function ($container) {
    return new \Slim\Csrf\Guard;
};

$container['validator'] = function ($container) {
    return new \App\Validation\Validator;
};

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(base_path('resources/views'), [
        'cache' => false
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));

    $view->getEnvironment()->addGlobal('assets_path', '/assets/');
    $view->getEnvironment()->addGlobal('upload_users', '/uploads/users/');
    $view->getEnvironment()->addGlobal('upload_posts', '/uploads/posts/');
    $view->getEnvironment()->addGlobal('upload_hospitals', '/uploads/hospitals/');
    $view->getEnvironment()->addGlobal('flash', $container->flash);
    $view->getEnvironment()->addGlobal('auth', $container->auth);
    $view->getEnvironment()->addGlobal('get_categories', Category::get());
    
    $mydump = new Twig_SimpleFilter('mydump', function ($var) {
        echo '<pre>';
            print_r($var);
        echo '<pre>';
    });
    $view->getEnvironment()->addFilter($mydump);

    /* GET SPECIALITIES IDS */
    $get_specialities_ids = new Twig_SimpleFilter('get_specialities_ids', function ($object_id, $object_type) {
        $specialities = SpecialityRelationship::where('object_id', $object_id)->where('object_type', $object_type)->get();
        if( $specialities != null ) {
            $ids = [];
            foreach( $specialities as $speciality ) {
                $ids[] = $speciality->speciality_id;
            }
            return $ids;
        }
        return null;
    });
    $view->getEnvironment()->addFilter($get_specialities_ids);
    /*******************/

    /* GET USER */
    $get_user = new Twig_SimpleFilter('get_user', function ($id) {
        $user = User::where('id', $id)->first();
        return $user;
    });
    $view->getEnvironment()->addFilter($get_user);
    /*******************/

    /* GET SPECIALITIES */
    $get_specialities = new Twig_SimpleFilter('get_specialities', function ($object_id, $object_type) {
        $specialities = SpecialityRelationship::where('object_id', $object_id)->where('object_type', $object_type)->get();
        return $specialities;
    });
    $view->getEnvironment()->addFilter($get_specialities);
    /*******************/

    /* GET SPECIALITY */
    $get_speciality = new Twig_SimpleFilter('get_speciality', function ($object_id, $object_type) {
        $specialities = SpecialityRelationship::where('object_id', $object_id)->where('object_type', $object_type)->get();
        return $specialities;
    });
    $view->getEnvironment()->addFilter($get_speciality);
    /*******************/

    /* SPECIALITIES */
    $specialities_list = new Twig_SimpleFilter('specialities_list', function () {
        $specialities_list = Speciality::get();
        return $specialities_list;
    });
    $view->getEnvironment()->addFilter($specialities_list);
    /*******************/

    /* GET HOSPITAL */
    $get_hospital = new Twig_SimpleFilter('get_hospital', function ($hospital_id) {
        $hospital = Hospital::where('id', $hospital_id)->get();
        return $hospital;
    });
    $view->getEnvironment()->addFilter($get_hospital);
    /*******************/

    /* GET USER META */
    $get_user_meta = new Twig_SimpleFilter('get_user_meta', function ( $user_id, $meta_key, $value_only = false ) {
        $user_meta = UserMeta::where('user_id', $user_id)->where('meta_key', $meta_key)->first();
        if( $user_meta != null ) {
            if( $value_only == true ) {
                $decode_value = json_decode($user_meta->meta_value);
                return $decode_value;
            }
            return json_decode($user_meta);
        }
        return null;
    });
    $view->getEnvironment()->addFilter($get_user_meta);
    /*******************/

    /* GET HOSPITAL META */
    $get_hospital_meta = new Twig_SimpleFilter('get_hospital_meta', function ( $hospital_id, $meta_key, $value_only = false ) {
        $hospital_meta = HospitalMeta::where('hospital_id', $hospital_id)->where('meta_key', $meta_key)->first();
        if( $hospital_meta != null ) {
            if( $value_only == true ) {
                $decode_value = json_decode($hospital_meta->meta_value);
                return $decode_value;
            }
            return json_decode($hospital_meta);
        }
        return null;
    });
    $view->getEnvironment()->addFilter($get_hospital_meta);
    /*******************/

    /* GET POST META */
    $get_post_meta = new Twig_SimpleFilter('get_post_meta', function ( $post_id, $meta_key, $value_only = false ) {
        $post_meta = PostMeta::where('post_id', $post_id)->where('meta_key', $meta_key)->first();
        if( $post_meta != null ) {
            if( $value_only == true ) {
                $decode_value = json_decode($post_meta);
                return $decode_value->meta_value;
            }
            return $post_meta;
        }
        return null;
    });
    $view->getEnvironment()->addFilter($get_post_meta);
    /*******************/

    /* GET CAT IDS */
    $get_cat_ids = new Twig_SimpleFilter('get_cat_ids', function ($post_id) {
        $categories = CategoryRelationship::where('object_id',$post_id)->get();
        if( $categories != null ) {
            $ids = [];
            foreach( $categories as $category ) {
                $ids[] = $category->category_id;
            }
            return $ids;
        }
        return null;
    });
    $view->getEnvironment()->addFilter($get_cat_ids);
    /*******************/
    
    return $view;
};

$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use($container) {
        return $container->view->render($response, 'templates/404.twig');
    };
};

$container['UserController'] = function ($container) {
    return new \App\Controllers\UserController($container);
};

$container['AppointmentController'] = function ($container) {
    return new \App\Controllers\AppointmentController($container);
};

$container['SpecialityController'] = function ($container) {
    return new \App\Controllers\SpecialityController($container);
};

$container['ArticleController'] = function ($container) {
    return new \App\Controllers\ArticleController($container);
};

$container['CategoryController'] = function ($container) {
    return new \App\Controllers\CategoryController($container);
};

$container['PageController'] = function ($container) {
    return new \App\Controllers\PageController($container);
};

$container['HospitalController'] = function ($container) {
    return new \App\Controllers\HospitalController($container);
};

$container['DoctorController'] = function ($container) {
    return new \App\Controllers\DoctorController($container);
};

$container['AuthController'] = function ($container) {
    return new \App\Controllers\AuthController($container);
};

$container['auth'] = function ($container) {
    return new \App\Auth\Auth;
};

$container['HomeController'] = function ($container) {
    return new \App\Controllers\HomeController($container);
};

$container['BlogController'] = function ($container) {
    return new \App\Controllers\BlogController($container);
};

$container['SecondOpinionController'] = function ($container) {
    return new \App\Controllers\SecondOpinionController($container);
};

$container['ContactController'] = function ($container) {
    return new \App\Controllers\ContactController($container);
};

$container['AboutController'] = function ($container) {
    return new \App\Controllers\AboutController($container);
};

$container['LocationController'] = function ($container) {
    return new \App\Controllers\LocationController($container);
};

$container['HowController'] = function ($container) {
    return new \App\Controllers\HowController($container);
};

$container['AdminController'] = function ($container) {
    return new \App\Controllers\AdminController($container);
};

$app->add( new \App\Middleware\ValidationErrorsMiddleware($container) );
$app->add( new \App\Middleware\OldInputMiddleware($container) );
$app->add( new \App\Middleware\CsrfViewMiddleware($container) );
$app->add($container->csrf);

v::With('App\\Validation\\Rules\\');

require base_path('routes/routes.php');